package com.tiviacz.pizzacraft.util;

public class Reference 
{
		public static final String MODID = "pizzacraft";
		public static final String NAME = "pizzacraft";
		public static final String VERSION = "1.1.6";
		public static final String ACCEPTED_VERSIONS = "[1.12.2]";
		public static final String CLIENT_PROXY_CLASS = "com.tiviacz.pizzacraft.proxy.ClientProxy";
		public static final String COMMON_PROXY_CLASS = "com.tiviacz.pizzacraft.proxy.CommonProxy";
}
